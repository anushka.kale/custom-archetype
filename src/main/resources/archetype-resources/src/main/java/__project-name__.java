package ${package};

import static com.genx.base.cicd.assemblyline.extn.jenkins.util.PipelineConstants.PROJECTNAME;

import java.io.IOException;
import java.io.StringReader;
import java.util.Objects;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.capgemini.dashboard.reusable.entity.CodeQualityInformationEntity;
import com.capgemini.dashboard.reusable.entity.FlagEntity;
import com.capgemini.dashboard.reusable.entity.JobInformationEntity;
import com.capgemini.dashboard.reusable.entity.RepositoryDetailsEntity;
import com.capgemini.genx.core.repository.IJenkinsCodeQualityInformationRepository;
import com.capgemini.genx.core.repository.IJenkinsJobInformationRepository;
import com.capgemini.genx.core.repository.IJenkinsRepositoryDetailsRepository;
import com.genx.base.cicd.dto.JobDTO;
import com.genx.base.cicd.dto.ToolsDTO;
import com.genx.base.cicd.assemblyline.extn.jenkins.serviceimpl.EnvironmentServiceImplNew;
import com.genx.base.cicd.assemblyline.extn.jenkins.util.DevOpsWorkFlowUtilNew;
import ${package}.JenkinsAbstractTool;


@Service("${project-name}")
public class ${project-name} extends JenkinsAbstractTool{


  @Autowired
  IJenkinsJobInformationRepository iJobInformationRepository;

  @Autowired
  EnvironmentServiceImplNew propertyUtil;

  @Autowired
  IJenkinsRepositoryDetailsRepository iJenkinsRepositoryDetailsRepository;

  @Autowired
  IJenkinsCodeQualityInformationRepository iJenkinsCodeQualityInformationRepository;

  @Autowired
  IJenkinsRepositoryDetailsRepository jenkinsRepositoryDetailsRepository;

  @Autowired
  IJenkins${project-name}JobRepository i${project-name}InformationRepository;

  @Autowired
  DevOpsWorkFlowUtilNew devOpsWorkFlowUtil;

  FlagEntity flagEntity=new FlagEntity();

  private static final Logger logger = LoggerFactory.getLogger(${project-name}.class);


	@Override
	public Boolean saveMetrics(JSONObject metrics, Long platformTypeId, Long toolId){

	  // TODO Auto-generated method stub
	}


}






