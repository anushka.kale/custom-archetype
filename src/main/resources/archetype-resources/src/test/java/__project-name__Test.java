package ${package};


import java.io.IOException;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.dashboard.reusable.entity.IstanbulEntity;
import com.capgemini.dashboard.reusable.entity.JobInformationEntity;
import com.capgemini.genx.core.repository.IJenkinsCodeQualityInformationRepository;
import com.capgemini.genx.core.repository.IJenkinsIstanbulRepository;
import com.capgemini.genx.core.repository.IJenkinsJobInformationRepository;
import com.capgemini.genx.core.repository.IJenkinsRepositoryDetailsRepository;
import com.genx.base.cicd.assemblyline.extn.jenkins.exception.DevOpsEnhancementException;
import com.genx.base.cicd.assemblyline.extn.jenkins.serviceimpl.EnvironmentServiceImplNew;
import com.genx.base.cicd.assemblyline.extn.jenkins.util.DevOpsWorkFlowUtilNew;
import com.genx.base.cicd.assemblyline.factory.ToolFactory;
import com.genx.base.cicd.assemblyline.tools.ITool;
import com.genx.base.cicd.dto.ToolsDTO;
import com.genx.base.cicd.exception.GenxCICDException;

public class ${project-name}Test {


    @InjectMocks
    ${project-name} ${project-name};

    @Mock
    private IJenkinsJobInformationRepository iJobInformationRepository;

    @Mock
    IJenkinsRepositoryDetailsRepository jenkinsRepositoryDetailsRepository;

    @Mock
    IJenkinsCodeQualityInformationRepository iJenkinsCodeQualityInformationRepository;

    @Mock
    IJenkinsRepositoryDetailsRepository iJenkinsRepositoryDetailsRepository;

    @Mock
    EnvironmentServiceImplNew propertyUtil;

    @Mock
    DevOpsWorkFlowUtilNew devOpsWorkFlowUtil;

    @Mock
    ToolsDTO toolsDTO;

    @Mock
    ToolFactory toolFactory;

    @Mock
    ITool iTool;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void saveMetricsTest() {
      // TODO Auto-generated method stub
}
